﻿using IronOcr;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ImageToText
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Visible = true;
            label2.Text = "Reading ...";
            GetText(openFileDialog1);
        }

        private void GetText(OpenFileDialog openFileDialog1)
        {            
            AutoOcr OCR = new AutoOcr() { ReadBarCodes = false };
            var Results = OCR.Read(openFileDialog1.FileName);
            Console.WriteLine(Results.Text);
            label2.Text = Results.Text;
        }

    }
}
